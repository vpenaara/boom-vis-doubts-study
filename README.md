# BOOM Doubts-Vis evaluation

This repository contains the instructions to participate in a user study using the BOOM visualization tool. Its main goal is to understand how Tephrochronologists can use the prototype to explore and analyze their analytical doubts. In the long term, it will allow us to gather feedback from the scientific community in order to improve the tool and leave it available to anyone who wants to use it.

We particularly interested in how Tephrochronologists identify samples that may require reclassification, geochemical anlysis that may need confirmation, volcanoes that need to be studied more, sources that are not trusted, etc. We call these "doubts" or "uncertainty" and they can both positive and negative, for example they can be a means to: identify open research questions, chose a subsets of the collected data whose complete analysis is more robust, or remove subsets of data from futher analysis.  

## Evaluation procedure

The study is divided in two two parts decribed below.

**Pre-interview**
Each participant shares with the researcher(s) a dataset of tephra samples related to their research. To create a dataset, please download [this template](/BOOM_dataset_template.csv) and fill it with your data. The template already contains one example row. For more information about the columns content, please consider the [description of each column](/BOOM_dataset_columns_description.md). This includes the option to add "doubts" (qualitative or quantitative). Please fill at least 20 rows of samples (contact us if you don't have that many). 

Once you have your dataset ready, send it back to the researcher and she will try to import it to the BOOM Vis. Please note that this process might need some iterations between the researcher and the participant. Once the dataset is successfully imported to the visualization tool, the researchers and the participant set a date to do an interview described next.

**Interview**
The interview will be conducted remotely or in person using a video conference tool to communicate with the researcher, who will be present for the whole interview. We will only record your screen and audio using the tool but not your face. 

The interview will be done in a single session and will last around 90 minutes. It consists of the following steps:
- Access to the tool: the researcher will provide the participant with a URL to the BOOM Vis where they can explore the previously supplied dataset.
- Tutorial of the tool: the researcher will explain all the interface components of the BOOM Vis to the participant. The participant will try it and get clarifications to any questions they may have before continuing to the following step.
- Exploration phase: the researcher will ask the participant to answer a set of questions using the BOOM Vis.
- Closing phase: the participant and the researcher(s) engage in a discussion about the use of the tool.

## Consent form

To participate in this study, we will ask you read and sign [this consent form](/consent_form.pdf). 