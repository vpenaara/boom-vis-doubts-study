# Description of the columns to create a dataset for the BOOM Vis

Each row of the dataset corresponds to a sample observation. The columns to be filled are described below:

## Mandatory columns
- `Sample ID`: Name of the sample.
- `SampleObservationID`: Name given to a specific observation/analysis of a sample. In cases when there is only one observation/analysis for sample, the Sample ID and Sample Observation ID coincide.
- `Longitude`: Longitude where the sample was collected.
- `Latitude`: Latitude where the sample was collected.
- `TypeOfRegister`: Type of material sampled. It might correspond to: “Organic matter”‚ “Pyroclastic material” or “Effusive material”.
- `TypeOfAnalysis`: Whether the analysis performed in the sample corresponds to "Bulk" (e.g., when bulk tephra is analyzed) or "Micro Analytical" (e.g., when individual glass shards are analyzed).
- `TypeOfSection`: Type of sedimentary archive where the sample was obtained from (e.g., "Outcrop"‚ "Lake core"‚ "Archaeological site").
- `Volcano`: Volcanic center indicated as the source of the volcanic deposit sampled. Use "Unknown" if the sample has not been assigned yet.
- `Event`: Name given to the specific eruptive event corresponding to the sampled volcanic deposit. Use "Unknown" if the sample has not been assigned yet.
- `Authors`: APA style reference of the publication where the sample information was originally published, e.g., "Martínez Fontaine et al., 2021".

## Optional columns

The following columns can  be left with empty values.

### Analytical doubts/uncertainty
If you have any doubts (e.g., qualitative) or calculated uncertainty/doubt values, please specify them as columns using the following format:

- `qualitative_doubt_1`: These columns have descriptive values (i.e. in text format) for doubts or uncertainty. If you have more than one, please add another column increasing the number at the end (e.g. `qualitative_doubt_2`, `qualitative_doubt_3`, `qualitative_doubt_4`).

- `quantitative_doubt_1`: These columns have numerical values for doubts or uncertainty. If you have more than one, please add another column increasing the number at the end (e.g. `quantitative_doubt_2`, `quantitative_doubt_3`, `quantitative_doubt_4`).

We will ask you to describe these values during the interview. Please note these are not mandatory. But if you have them, please add them to at least 5 rows (5 sample observations) to describe the doubts/uncertainties you have about them so we can reflect on them during the interview.

### Normalized geochemistry:
The following columns represent the normalized major element composition of the analyzed volcanic products: 

- ```SiO2_normalized, TiO2_normalized,Al2O3_normalized, FeO_normalized, Fe2O3_normalized, Fe2O3T_normalized, FeOT_normalized, MnO_normalized, MgO_normalized, CaO_normalized, Na2O_normalized, K2O_normalized, P2O5_normalized, Cl_normalized, Total_normalization```

Fill at least two of these columns if you want to see bivariate plots in the tool. Feel free to add non-normalized values, even if in the name suggest a normalization.

### Age
- `RadiocarbonAge`: Non-calibrated radiocarbon years Before Present (BP) of organic matter associated to a specific tephra.
- `RadiocarbonAgeError`: Analytical error (1σ) for the radiocarbon age.
- `HistoricalAge`: Year CE when the eruption producing the deposit occurred.

## Other columns displayed in the tool
- `DOI`: Digital Object Identifier of the publication where the sample information was originally published. In cases where the DOI link is broken or no DOI has been assigned to the publication, a web site where the publication can be accessed is provided instead. 
- `Standard`: Comma separated names of the standards used in the analysis.
- `Magnitude`: Magnitude of the eruptive event (Pyle, 2000; 2015) associated with the sampled volcanic deposit, as estimated in the original publication. Different publications might estimate different Magnitudes for the same event.
- `VEI`: Volcanic Explosivity Index (Newhall and Self, 1982) of the eruptive event associated with the sampled volcanic deposit, as estimated in the original publication. Different publications might estimate different VEIs for the same event.
- `Flag`: Comma separated flag names (e.g. "Geochemistry_Issue", "SampleID_Issue") to indicate any problem in the sample.

Please note that the original dataset contains a longer list of columns. We use this subset only for the sake of the evaluation.